// console.log('hola')

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))


// to do list items

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
	let list = json.map((todo => {
		return todo.title;
	}))
	console.log(list)
})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


// todo list using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
 	'Content-Type': 'application/json'
 	},
 	body: JSON.stringify({
 	title: 'Created To Do List Item',
 	completed: false,
 	userId: 1
 	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// to do list PUT method


	fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
 	'Content-Type': 'application/json'
 	},
 	body: JSON.stringify({
 	title: 'Updated To Do List Item',
 	description: "To update the my to do list with a different data structure.",
 	status: 'Pending',
 	dataCompleted: 'Pending',
 	userId: 1
 })
})
.then((response) => response.json())
.then((json) => console.log(json))

// to do list using patch method

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
		dateCompleted: '07/09/21'
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// DELETE
fetch('https://jsonplaceholder.typicode.com/posts/1', {
method: 'DELETE',
})