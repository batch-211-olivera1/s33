// Application Programming Interface

// API part of server responsible for receiving request and sending responses

/*
	-What is it for
	-hides the server beneath an interface layer
	-Hidden complexity makes apps easier to use
	-sets rules of inreraction bet. front and back end of application, improving security
*/

// GUI
// Command line interface

// what is REST?
	/*
		-Representational state transfer
		-architectural style for standards for communication
		-the need to separate user interface concerns of the client from data storage concerns of the server
		*how does it solve
		-1. Statelessness
			-does not need to know client state and vice versa
			-made possible via resources and HTTP method
		-2. standardize Communication
			-enables decoupled server to understand, process, and respond to client requests without knowing client state
			-implemented via resources represented as uniform resource indentifier(URI) endpoints and HTTP methods
	*/

	// Rest API methods
	/*
	anatomy of client request
	-performed dictated by HTTP Methods
		1. GET
		2. POST
		3. PUT
		4. DELETE
	-A PATH to the resource to be operated on
		-URI endpoint
	-A header containing additional request information
	-A request BODY containing data(optional)
	*/


// console.log('Hello first')



// JavaScript Synchronous and asynchronous
 /*
	-synchronous codes runs in sequence. the means that each operation must wait for the previous one to complete before executing
		-Code blocking

	Asynchronous code runs in parallel. this means that an operation can occur while another one is still being processed.

	Asynchronous code execution is often preferrable in situations where execution can be blocked. some examples of this are network requests,
	long-running calculations, file system operations, et. using asynchronous code in the browser ensures the page remains response and the user experience is mostly unaffected
 */

 // example Synchronous
 	console.log("Hello second");
 	// cosnole.log("Hello third")
 	// for (let i = 0; i <= 100; i++){
 	// 	console.log(i + " Hello")
 	// };
 	console.log("Hello fourth");


 	/*
		-API is a particular set of codes that allows software to communicate with each other
		-API is the interface through which you access someone else's code or through which someone else's code accesses yours.

		Example:
			Google APIs
				https://developers.google.com/identity/sign-in/web/sign-in

			Youtube APIs
				https://developers.google.com/youtube/iframe_api_reference
 	*/

 	// fetch API

 	// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
 	/*
		-A "promise" is an object that represents the eventual completion or failure of an asychronous function and its resulting value

		-A promise is in one of these three states: 
		Pending:
		-initial state, neither fulfilled nor rejected
		fulfilled:
		-Operation was successfully completed
		Rejected:
		-Operation failed
 	*/

 	/*
		-using the ".then" method, we can now check for the status of the promise
		-the "fetch" method will return a "promise" that resolves to be a "response" object
		-the ".then" method which will eventually be "resolved" or "rejected"
		Syntax:
			fetch('URL')
			.then((response) => {})
 	*/



 	// fetch('https://jsonplaceholder.typicode.com/posts')
 	// .then(response => console.log(response.status))



 	// fetch('https://jsonplaceholder.typicode.com/posts')
 	// // used the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
 	// .then((response) => response.json())
 	// // print the converted JSON value from the "fetch" request
 	// // using multiple ".then" method to create a promise chain
 	// .then((json) => console.log(json))


 	/*
		-the "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
		-Used in function s to indicate which portions of code shopuld be waited
		-creates an asynchronous functions

 	*/

 	// async function fetchData () {

 	// 	// waits for the "fetch" method to complere then stores the value in the "result" variable
 	// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

 	// 	// the result returned by "fetch" is returned as a promise 
 	// 	console.log(result)

 	// 	// the returned "response" is an object
 	// 	console.log(typeof result);

 	// 	// We cannot access the content of the "response" by directly accessing it's body property
 	// 	console.log(result.body)
 	// 	let json = await result.json();
 	// 	console.log(json)

 	// };
 	// fetchData();

 	// Getting a specific POST

 	/*
		-retrieve a specific post following the Rest API (retrieve, /posts/id:, GET)
 	*/

 	// fetch('https://jsonplaceholder.typicode.com/posts/1')
 	// .then((response) => response.json())
 	// .then((json) => console.log(json))

 // Postman

 /*
	-url 'https://jsonplaceholder.typicode.com/posts/1'
	-GET method

 */ 

 // Create POST
 /*
	-Syntax:
		fetch('url', options)
		.then((response) => {})
		.then((response) => {})
 */

 // create a new post following the rest API (create, /post/:id, POST)
 	// fetch('https://jsonplaceholder.typicode.com/posts', {
 	// 	method: 'POST',
 	// 	headers: {
 	// 		'Content-Type': 'application/json'
 	// 	},
 	// 	body: JSON.stringify({
 	// 		title: 'New post',
 	// 		body: 'Hello World',
 	// 		userId: 1
 	// 	})
 	// })
 	// .then((response) => response.json())
 	// .then((json) => console.log(json))

 	// Postman
 	/*
		-url: https://jsonplaceholder.typicode.com/posts
		-method: POST
		-body: raw + JSON
			{
				"title": "My First Blog Post",
				"body": "Hello World",
				"userId": 1
			}
 	*/

 // Updating a post
 /*
	- updates a specific post following the rest API (update, /posts/:id, PUT)

 */
 	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
 	// 	method: 'PUT',
 	// 	headers: {
 	// 		'Content-Type': 'application/json'
 	// 	},
 	// 	body: JSON.stringify({
 	// 		id: 1,
 	// 		title: 'Updated post',
 	// 		body: 'Hello again!',
 	// 		userId: 1
 	// 	})
 	// })
 	// .then((response) => response.json())
 	// .then((json) => console.log(json))

/*
	-postman:

		url: https://jsonplaceholder.typicode.com/posts/1
		Method: PUT
		body: raw + JSON
			{
				"title": "My first revised blog post",
				"body": "Hello there! I revised this a bit",
				"userId": 1
			}
*/

// Update a post using PATCH method
/*
	-update a specific post following the rest API (update, /posts/:id, Patch)
	-the difference btween PUT and PATCH is the number of properties being changed
	-PATCH is used to update single/several properties
	-PUT is used to update the whole object
*/

// fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method: 'PATCH',
// 	header: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: 'Corrected post'
// 	})
// })

// 	.then((response) => response.json())
//  	.then((json) => console.log(json))

 /*
	postman:
	url: https://jsonplaceholder.typicode.com/posts/1
	Method: PATCH
	body: raw + json
		{
			"title": "This is my final title"
		}
 */


 // DELETING a post
 /*
	-deleting a specific post following the rest API (delete, /post/:id, DELETE)

 */
//  	fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method: 'DELETE',
// })

/*
	postman:
		url: https://jsonplaceholder.typicode.com/posts/1
		method: DELETE
*/

// Filtering the post
/*
	-the data can be filtered by sending the userId along with the URL
	-Information sent via the URL can be done by adding the question maek
	symbol (?)
	Syntax:
		Individual Parameters:
			'url?parameterName=value'
		Multiple Parameters:
			'url?ParamA=valueA&paramB=valueB'
*/

	// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	// .then((response) => response.json())
 // 	.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3&userId=4')
.then((response) => response.json())
.then((json) => console.log(json))


// Retrieve comments of a specific post
/*
	-following the rest API (retrieve, /posts/id:, GET)
*/

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json))